package me.tommyli.rag;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.apache.commons.lang3.RandomStringUtils;

public class RandomAGui extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JTextField tf, no;
	private JComboBox<String> jcb;
	
	public RandomAGui(){
		super("RandomAG v1.0");
		setLayout(new FlowLayout());
		add(new JLabel("Random Answer Generator by Tommy Li"));
		
		String[] type = {
				"Open Question",
				"T or F",
				"ABC",
				"ABCD",
				"ABCDE"
		};
		jcb = new JComboBox<String>(type);
		add(jcb);
		
		no = new JTextField("5", 2);
		no.setHorizontalAlignment(JTextField.CENTER);
		add(no);
		
		tf = new JTextField(30);
		tf.setEditable(false);
		tf.setHorizontalAlignment(JTextField.CENTER);
		add(tf);
		
		JButton gen = new JButton("Generate!");
		add(gen);
		
		gen.addActionListener(new ActionHandler());
	}
	
	private class ActionHandler implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			RandomUtils r = new RandomUtils();
			int idx = jcb.getSelectedIndex();
			ArrayList<String> sa = new ArrayList<String>(); 
			int max = Integer.parseInt(no.getText());
			for (int i = 0; i < max; i++){
				if (idx == 0)
					sa.add(RandomStringUtils.randomAlphanumeric(5));
				else if (idx == 1)
					sa.add(r.getTF());
				else
					sa.add(r.getABCD(idx));
			}
			Iterator<String> it = sa.iterator();
			String s = it.next();
			while (it.hasNext()){
				s += " " + it.next();
			}
			tf.setText(s);
		}
		
	}
	
}
