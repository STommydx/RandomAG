package me.tommyli.rag;

import javax.swing.JFrame;

public class RandomAG {
	
	public static void main(String args[]){
		RandomAGui gui = new RandomAGui();
		gui.setSize(400, 150);
		gui.setResizable(false);
		gui.setVisible(true);
		gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gui.setAlwaysOnTop(true);
	}

}
