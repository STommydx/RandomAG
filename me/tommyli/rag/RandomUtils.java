package me.tommyli.rag;

import java.util.Random;

public class RandomUtils {

	Random r;

	public RandomUtils(Random rnd) {
		r = rnd;
	}

	public RandomUtils() {
		this(new Random());
	}

	public String getTF() {
		if (r.nextBoolean())
			return "TRUE";
		else
			return "FALSE";
	}
	
	public String getABCD(int n){
		String[] a = {
				"A",
				"B",
				"C",
				"D",
				"E",
				"F"
		};
		return a[r.nextInt(n+1)];
	}
	
}
